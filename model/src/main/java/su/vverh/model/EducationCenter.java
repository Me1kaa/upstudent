package su.vverh.model;

import javax.persistence.*;

/**
 * Created by shirval on 02.03.16.
 */
@Entity
@Table(name = "EDUCATION_CENTER")
public class EducationCenter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "EDUCATION_CENTER_TYPE_ID")
    private EducationCenterType type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EducationCenterType getType() {
        return type;
    }

    public void setType(EducationCenterType type) {
        this.type = type;
    }
}
