package su.vverh.model;

import javax.persistence.*;

/**
 * Created by shirval on 02.03.16.
 */
@Entity
@Table(name = "EDUCATION_CENTER_TYPE")
public class EducationCenterType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
