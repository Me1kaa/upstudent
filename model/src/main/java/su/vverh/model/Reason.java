package su.vverh.model;

import javax.persistence.*;

/**
 * Created by shirval on 01.03.16.
 */
@Entity
@Table(name = "REASON")
public class Reason {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String reason;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
