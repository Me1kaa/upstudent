package su.vverh.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by shirval on 02.03.16.
 */
@Entity
@Table(name = "USER_GROUP")
public class UserGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;
    private String description;

    @ManyToMany(fetch = FetchType.EAGER)//todo change to lazy
    @JoinTable(name = "GROUP_ACCESS",
    joinColumns = @JoinColumn(name = "GROUP_ID"),
    inverseJoinColumns = @JoinColumn(name = "USER_ROLE_ID"))
    List<UserRole> roles;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(List<UserRole> roles) {
        this.roles = roles;
    }
}
