package su.vverh.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by shirval on 01.03.16.
 */
@Entity
@Table(name = "STUDENT",
uniqueConstraints = @UniqueConstraint(columnNames = {"FIRST_NAME", "MIDDLE_NAME", "LAST_NAME", "DATE_OF_BIRTH", "PASSPORT_SERIA", "PASSPORT_NUMBER"}))
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;
    @Column(name = "MIDDLE_NAME", nullable = false)
    private String middleName;
    @Column(name = "LAST_NAME", nullable = false)
    private String lastName;
    @Column(name = "GENDER", nullable = false, length = 1)
    private String gender;
    @Column(name = "DATE_OF_BIRTH", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;

    @ManyToOne
    @JoinColumn(name = "PLACE_OF_BIRTH")
    private Adress placeOfBirth;

    @Column(name = "EMAIL")
    private String email;
    @Column(name = "CONTACT_NUMBER")
    private String contactNumber;

    @ManyToOne
    @JoinColumn(name = "STATUS")
    private MaritalStatus status;

    @Column(name = "PASSPORT_SERIA", nullable = false)
    private String passportSeria;
    @Column(name = "PASSPORT_NUMBER", nullable = false)
    private String passportNumber;
    @Column(name = "PASSPORT_PLACE")
    private String passportPlace;
    @Column(name = "PASSPORT_DATE_START")
    @Temporal(TemporalType.DATE)
    private Date passportDateStart;
    @Column(name = "REGISTRATION")
    private String registration;
    @Column(name = "ADRESS_FACT")
    private String addresFact;
    @Column(name = "TRUSTED")
    private String trused;

    @ManyToOne
    @JoinColumn(name = "REASON")
    private Reason reason;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Adress getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(Adress placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public MaritalStatus getStatus() {
        return status;
    }

    public void setStatus(MaritalStatus status) {
        this.status = status;
    }

    public String getPassportSeria() {
        return passportSeria;
    }

    public void setPassportSeria(String passportSeria) {
        this.passportSeria = passportSeria;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPassportPlace() {
        return passportPlace;
    }

    public void setPassportPlace(String passportPlace) {
        this.passportPlace = passportPlace;
    }

    public Date getPassportDateStart() {
        return passportDateStart;
    }

    public void setPassportDateStart(Date passportDateStart) {
        this.passportDateStart = passportDateStart;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getAddresFact() {
        return addresFact;
    }

    public void setAddresFact(String addresFact) {
        this.addresFact = addresFact;
    }

    public String getTrused() {
        return trused;
    }

    public void setTrused(String trused) {
        this.trused = trused;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", passportSeria='" + passportSeria + '\'' +
                ", passportNumber='" + passportNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (id != student.id) return false;
        if (!firstName.equals(student.firstName)) return false;
        if (!middleName.equals(student.middleName)) return false;
        if (!lastName.equals(student.lastName)) return false;
        if (!gender.equals(student.gender)) return false;
        if (!dateOfBirth.equals(student.dateOfBirth)) return false;
        if (!passportSeria.equals(student.passportSeria)) return false;
        return passportNumber.equals(student.passportNumber);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + firstName.hashCode();
        result = 31 * result + middleName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + gender.hashCode();
        result = 31 * result + dateOfBirth.hashCode();
        result = 31 * result + passportSeria.hashCode();
        result = 31 * result + passportNumber.hashCode();
        return result;
    }
}
