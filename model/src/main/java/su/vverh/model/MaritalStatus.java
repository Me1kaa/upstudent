package su.vverh.model;

import javax.persistence.*;

/**
 * Created by shirval on 01.03.16.
 */
@Entity
@Table(name = "MARITAL_STATUS")
public class MaritalStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
