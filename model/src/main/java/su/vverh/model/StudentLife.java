package su.vverh.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by shirval on 02.03.16.
 */
@Entity
@Table(name = "STUDENT_LIFE")
public class StudentLife {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "STUDENT_ID")
    private Student student;

    @Column(name = "START_DATE")
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Column(name = "END_DATE")
    @Temporal(TemporalType.DATE)
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "LIVING_CENTER_ID")
    private LivingCenter livingCenter;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public LivingCenter getLivingCenter() {
        return livingCenter;
    }

    public void setLivingCenter(LivingCenter livingCenter) {
        this.livingCenter = livingCenter;
    }
}
