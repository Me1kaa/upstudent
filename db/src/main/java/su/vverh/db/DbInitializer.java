package su.vverh.db;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * Created by shirval on 02.03.16.
 */
public class DbInitializer {

    public static void main(String[] args) throws Exception {
        System.out.println("*****************************");
        System.out.println("*      Initializing DB      *");
        System.out.println("*****************************");
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:config/db-initializer-config.xml");
        System.out.println("*****************************");
        System.out.println("*       DB initialized      *");
        System.out.println("*****************************");
    }

}
