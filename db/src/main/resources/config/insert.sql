insert into user_roles
(id, name, description)
VALUES
(1, 'ADMIN', 'Full Access');

insert into user_group
(id, name, description)
VALUES
(1, 'ADMINS', 'Full Access');

insert into GROUP_ACCESS
(GROUP_ID, USER_ROLE_ID)
VALUES
(1, 1);

insert into users
(id, name, login, password, group_id)
values
(1, 'SUPER USER', 'admin', 'admin', 1);