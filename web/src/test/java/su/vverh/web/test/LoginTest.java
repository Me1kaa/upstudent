package su.vverh.web.test;

import org.hamcrest.core.StringEndsWith;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithSecurityContextTestExecutionListener;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.ServletTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import su.vverh.service.UpUserDetailsService;
import su.vverh.service.UserService;

import javax.servlet.Filter;
import java.util.Arrays;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;



/**
 * Created by 1 on 26.03.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:web-test-context.xml")
@TestExecutionListeners(listeners={ServletTestExecutionListener.class,
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        WithSecurityContextTestExecutionListener.class})
public class LoginTest {

    @Autowired
    private WebApplicationContext webAppContext;
    private MockMvc mockMvc;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private UpUserDetailsService userDetailsService;


    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).addFilters(springSecurityFilterChain).build();
        when(userDetailsService.loadUserByUsername("user")).thenReturn(getMockUserDetails());
    }

    @Test
    public void testAuth_redirect_to_login_when_not_authenticated() throws Exception {

        MvcResult mvcResult = mockMvc.perform(get("/home"))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", StringEndsWith.endsWith("/login")))
                .andReturn();
    }

    @Test
    public void testAuth_incorrect_user_and_password() throws Exception {
        MvcResult mvcResult = mockMvc.perform(formLogin("/login")
                .user("notexist").password("invalid"))
                .andExpect(header().string("Location", StringEndsWith.endsWith("/login?error")))
                .andReturn();
    }

    @Test
    public void testAuth_forward_to_home_after_authentication() throws Exception {
        MvcResult mvcResult = mockMvc.perform(formLogin("/login")
                .user("user").password("password"))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", StringEndsWith.endsWith("/home")))
                .andReturn();
    }

    @Test
    public void testAuth_forward_to_requested_page_after_authentication() throws Exception {
        mockMvc.perform(get("/home").with(user("user").password("password")))
                .andExpect(status().isOk())
                .andReturn();
    }


    private UserDetails getMockUserDetails() {
        return new User("user", "password", Arrays.asList(new SimpleGrantedAuthority("USER")));
    }

}

