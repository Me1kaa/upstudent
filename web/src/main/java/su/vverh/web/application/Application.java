package su.vverh.web.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by shirval on 17.03.16.
 */
@SpringBootApplication
@ComponentScan({"su.vverh.web"})
public class Application {

    public static void main(String[] args) {
        //see Tomcat port in application.properties
        SpringApplication.run(Application.class, args);
    }
}
