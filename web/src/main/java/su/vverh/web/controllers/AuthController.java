package su.vverh.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import su.vverh.model.PersistentLogins;
import su.vverh.service.PersistentLoginsService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 1 on 23.03.2016.
 */
@Controller
public class AuthController {

    @RequestMapping(path = "/login")
    public ModelAndView getLoginPage() {
        return new ModelAndView("login");
    }

    @RequestMapping(path = {"/home", "/"})
    public ModelAndView getHomePage(@RequestParam Map<String, String> params) {
        return new ModelAndView("home");
    }
}
