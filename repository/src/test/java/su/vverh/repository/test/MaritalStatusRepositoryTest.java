package su.vverh.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import su.vverh.model.MaritalStatus;
import su.vverh.repository.MaritalStatusRepository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by 1 on 06.03.2016.
 */
@ContextConfiguration(locations = "classpath:repository-test-context.xml")
public class MaritalStatusRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests{
    
    @Autowired
    private MaritalStatusRepository maritalStatusRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void insert_test_entity_exist_after_insert() {
        String name = "MARRIED";
        createOneRow(name);
        MaritalStatus status = maritalStatusRepository.findOne(1);
        Assert.assertNotNull(status);
        Assert.assertEquals(name, status.getStatus());
    }

    @Test
    public void findAll_test_returns_all_rows() {
        String[] statuses = new String [] {"NAME1", "NAME2", "NAME3"};
        createSeveralRows(statuses);
        int count = countRowsInTable("MARITAL_STATUS");
        List<MaritalStatus> centers = maritalStatusRepository.findAll();
        Assert.assertEquals(count, centers.size());
    }

    @Test
    public void save_test_saves_new_entity() {
        String name = "MARRIED";
        MaritalStatus status = new MaritalStatus();
        status.setStatus(name);
        maritalStatusRepository.save(status);
        Assert.assertTrue(status.getId() > 0);
        MaritalStatus savedStatus = entityManager.find(MaritalStatus.class, status.getId());
        Assert.assertNotNull(savedStatus);
        Assert.assertEquals(name, savedStatus.getStatus());

    }

    @Test
    public void save_test_updates_existing_entity() {
        String oldName = "NOT MARRIED";
        String newName = "MARRIED";
        createOneRow(oldName);
        MaritalStatus status = entityManager.find(MaritalStatus.class, 1);
        Assert.assertNotNull(status);
        Assert.assertEquals(oldName, status.getStatus());

        status.setStatus(newName);
        maritalStatusRepository.save(status);
        MaritalStatus updatedStatus = entityManager.find(MaritalStatus.class, 1);
        Assert.assertNotNull(updatedStatus);
        Assert.assertNotEquals(oldName, updatedStatus.getStatus());
        Assert.assertEquals(newName, updatedStatus.getStatus());
    }

    @Test
    public void delete_test_delete_saved_entity() {
        String[] names = new String [] {"NAME1", "NAME2", "NAME3"};
        createSeveralRows(names);
        int oldCount = countRowsInTable("MARITAL_STATUS");
        MaritalStatus status = entityManager.find(MaritalStatus.class, 1);
        Assert.assertNotNull(status);
        maritalStatusRepository.delete(status);
        maritalStatusRepository.flush();
        status = entityManager.find(MaritalStatus.class, 1);
        Assert.assertNull(status);
        int newCount = (countRowsInTable("MARITAL_STATUS"));
        Assert.assertEquals(oldCount, newCount + 1);

    }

    private void createSeveralRows(String ... statuses) {
        String sql = "INSERT INTO MARITAL_STATUS (ID, STATUS) " +
                " VALUES " +
                " (?, ?)";
        int count = 1;
        for (String status : statuses) {
            jdbcTemplate.update(sql, count, status);
            count++;
        }
    }
    private void createOneRow(String status) {
        String sql = "INSERT INTO MARITAL_STATUS (ID, STATUS) " +
                " VALUES " +
                " (1, ?)";
        jdbcTemplate.update(sql, status);
    }
}
