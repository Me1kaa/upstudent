package su.vverh.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import su.vverh.model.Speciality;
import su.vverh.repository.SpecialityRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by 1 on 06.03.2016.
 */
@ContextConfiguration(locations = "classpath:repository-test-context.xml")
public class SpecialityRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests{

    @Autowired
    private SpecialityRepository specialityRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void insert_test_entity_exist_after_insert() {
        String name = "ENGINEER";
        createOneRow(name);
        Speciality speciality = specialityRepository.findOne(1);
        Assert.assertNotNull(speciality);
        Assert.assertEquals(name, speciality.getName());
    }

    @Test
    public void findAll_test_returns_all_rows() {
        String [] names = new String[] {"S1", "S2", "S3"};
        createSeveralRows(names);
        int rows = countRowsInTable("SPECIALITY");
        List<Speciality> specialities = specialityRepository.findAll();
        Assert.assertEquals(rows, specialities.size());
    }

    @Test
    public void save_test_saves_new_entity() {
        String name = "ENGINEER";
        Speciality speciality = new Speciality();
        speciality.setName(name);

        specialityRepository.save(speciality);
        Assert.assertTrue(speciality.getId() > 0);
        Speciality savedSpeciality = entityManager.find(Speciality.class, speciality.getId());
        Assert.assertNotNull(savedSpeciality);
        Assert.assertEquals(name, savedSpeciality.getName());
    }

    @Test
    public void save_test_updates_existing_entity() {
        String oldName = "ECONOMIST";
        String newName = "ENGINEER";
        createOneRow(oldName);
        Speciality speciality = entityManager.find(Speciality.class, 1);
        Assert.assertNotNull(speciality);
        Assert.assertEquals(oldName, speciality.getName());

        speciality.setName(newName);
        specialityRepository.save(speciality);
        speciality = entityManager.find(Speciality.class, 1);
        Assert.assertNotNull(speciality);
        Assert.assertNotEquals(oldName, speciality.getName());
        Assert.assertEquals(newName, speciality.getName());
    }

    @Test
    public void delete_test_delete_saved_entity() {
        String [] names = new String[] {"S1", "S2", "S3"};
        createSeveralRows(names);
        int oldRows = countRowsInTable("SPECIALITY");
        Speciality speciality = entityManager.find(Speciality.class, 1);
        Assert.assertNotNull(speciality);

        specialityRepository.delete(speciality);
        specialityRepository.flush();
        speciality = entityManager.find(Speciality.class, 1);
        Assert.assertNull(speciality);
        int newRows = countRowsInTable("SPECIALITY");
        Assert.assertEquals(oldRows, newRows + 1);
    }

    private void createSeveralRows(String ... names) {
        String sql = "INSERT INTO SPECIALITY (ID, NAME) VALUES (?, ?)";
        int count = 1;
        for (String name : names) {
            jdbcTemplate.update(sql, count, name);
            count++;
        }
    }
    private void createOneRow(String name) {
        String sql = "INSERT INTO SPECIALITY (ID, NAME) VALUES (1, ?)";
        jdbcTemplate.update(sql, name);
    }
}
