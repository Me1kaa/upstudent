package su.vverh.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import su.vverh.model.LivingCenter;
import su.vverh.repository.LivingCenterRepository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by 1 on 06.03.2016.
 */
@ContextConfiguration(locations = "classpath:repository-test-context.xml")
public class LivingCenterRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private LivingCenterRepository livingCenterRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void insert_test_entity_exist_after_insert() {
        String name = "ORPHANAGE OF SAINT-FEODORA";
        createOneRow(name);
        LivingCenter center = livingCenterRepository.findOne(1);
        Assert.assertNotNull(center);
        Assert.assertEquals(name, center.getName());
    }

    @Test
    public void findAll_test_returns_all_rows() {
        String[] names = new String [] {"NAME1", "NAME2", "NAME3"};
        createSeveralRows(names);
        int count = countRowsInTable("LIVING_CENTER");
        List<LivingCenter> centers = livingCenterRepository.findAll();
        Assert.assertEquals(count, centers.size());
    }

    @Test
    public void save_test_saves_new_entity() {
        String name = "ORPHANAGE OF SAINT-FEODORA";
        LivingCenter center = new LivingCenter();
        center.setName(name);
        livingCenterRepository.save(center);
        Assert.assertTrue(center.getId() > 0);
        LivingCenter savedCenter = entityManager.find(LivingCenter.class, center.getId());
        Assert.assertNotNull(savedCenter);
        Assert.assertEquals(name, savedCenter.getName());

    }

    @Test
    public void save_test_updates_existing_entity() {
        String oldName = "ORPHANAGE OF SAINT-FEODORA";
        String newName = "KINDERGATEN OF PUTIN";
        createOneRow(oldName);
        LivingCenter center = entityManager.find(LivingCenter.class, 1);
        Assert.assertNotNull(center);
        Assert.assertEquals(oldName, center.getName());

        center.setName(newName);
        livingCenterRepository.save(center);
        LivingCenter updatedCenter = entityManager.find(LivingCenter.class, 1);
        Assert.assertNotNull(updatedCenter);
        Assert.assertNotEquals(oldName, updatedCenter.getName());
        Assert.assertEquals(newName, updatedCenter.getName());
    }

    @Test
    public void delete_test_delete_saved_entity() {
        String[] names = new String [] {"NAME1", "NAME2", "NAME3"};
        createSeveralRows(names);
        int oldCount = countRowsInTable("LIVING_CENTER");
        LivingCenter center = entityManager.find(LivingCenter.class, 1);
        Assert.assertNotNull(center);
        livingCenterRepository.delete(center);
        livingCenterRepository.flush();
        center = entityManager.find(LivingCenter.class, 1);
        Assert.assertNull(center);
        int newCount = (countRowsInTable("LIVING_CENTER"));
        Assert.assertEquals(oldCount, newCount + 1);

    }

    private void createSeveralRows(String ... names) {
        String sql = "INSERT INTO LIVING_CENTER (ID, NAME) " +
                " VALUES " +
                " (?, ?)";
        int count = 1;
        for (String name : names) {
            jdbcTemplate.update(sql, count, name);
            count++;
        }
    }
    private void createOneRow(String name) {
        String sql = "INSERT INTO LIVING_CENTER (ID, NAME) " +
                " VALUES " +
                " (1, ?)";
        jdbcTemplate.update(sql, name);
    }
}
