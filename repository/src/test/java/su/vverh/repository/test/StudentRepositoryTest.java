package su.vverh.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import su.vverh.model.Adress;
import su.vverh.model.MaritalStatus;
import su.vverh.model.Reason;
import su.vverh.model.Student;
import su.vverh.repository.AdressRepository;
import su.vverh.repository.MaritalStatusRepository;
import su.vverh.repository.ReasonRepository;
import su.vverh.repository.StudentRepository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

/**
 * Created by shirval on 01.03.16.
 */
@ContextConfiguration(locations = "classpath:repository-test-context.xml")
public class StudentRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests{

    @Autowired
    private StudentRepository studentRepository;
    @PersistenceContext
    private EntityManager entityManager;


    @Test
    public void insert_test_entity_exist_after_insert() {
        Student testStudent = createTestStudent();
        createOneRow(testStudent);
        Student student = studentRepository.findOne(1);
        Assert.assertNotNull(student);
        Assert.assertEquals(testStudent.getFirstName(), student.getFirstName());
        Assert.assertEquals(testStudent.getPassportSeria(), student.getPassportSeria());
    }

    @Test
    public void findAll_test_returns_all_rows() {
        Student [] students = createSeveralTestStudents();
        createSeveralRows(students);
        List<Student> savedStudents = studentRepository.findAll();
        Assert.assertEquals(students.length, savedStudents.size());

    }

    @Test
    public void save_test_saves_new_entity() {
        Student student = createTestStudent();
        studentRepository.save(student);
        Assert.assertTrue(student.getId() > 0);
        Student savedStudent = entityManager.find(Student.class, student.getId());
        Assert.assertNotNull(savedStudent);
        Assert.assertEquals(student.getFirstName(), savedStudent.getFirstName());
        Assert.assertEquals(student.getPassportNumber(), savedStudent.getPassportNumber());
    }

    @Test
    public void save_test_updates_existing_entity() {
        Student student = createTestStudent();
        createOneRow(student);
        Student savedStudent = entityManager.find(Student.class, 1);
        Assert.assertNotNull(savedStudent);
        Assert.assertEquals(student.getFirstName(), savedStudent.getFirstName());

        String newName = "newName";
        savedStudent.setFirstName(newName);
        studentRepository.save(savedStudent);
        Student updatedStudent = entityManager.find(Student.class, 1);
        Assert.assertNotNull(updatedStudent);
        Assert.assertNotEquals(student.getFirstName(), updatedStudent.getFirstName());
        Assert.assertEquals(newName, updatedStudent.getFirstName());
    }

    @Test
    public void delete_test_delete_saved_entity() {
        Student[] students = createSeveralTestStudents();
        createSeveralRows(students);
        int size = students.length;
        int count = countRowsInTable("STUDENT");
        Assert.assertEquals(size, count);

        Student student = entityManager.find(Student.class, 1);
        Assert.assertNotNull(student);

        studentRepository.delete(student);
        studentRepository.flush();
        student = entityManager.find(Student.class, 1);
        Assert.assertNull(student);

        count = countRowsInTable("STUDENT");
        Assert.assertEquals(size, count + 1);
    }

    private Student createStudent (String firstName, String middleName, String lastName,
                                   String gender, Date dateOfBirth, String passportSeria, String passportNumber) {
        Student student = new Student();
        student.setFirstName(firstName);
        student.setMiddleName(middleName);
        student.setLastName(lastName);
        student.setGender(gender);
        student.setDateOfBirth(dateOfBirth);
        student.setPassportSeria(passportSeria);
        student.setPassportNumber(passportNumber);
        return student;
    }

    private Student createTestStudent() {
        String firstName = "first";
        String middleName = "middle";
        String lastName = "last";
        String gender = "m";
        Date date = new Date();
        String passportSeria = "0123";
        String passportNumber = "012345";
        return createStudent(firstName, middleName, lastName, gender, date, passportSeria, passportNumber);
    }

    private Student[] createSeveralTestStudents() {
        Student student1 = createTestStudent();
        Student student2 = createStudent("test1", "test1", "test1", "f", new Date(), "4444", "666666");
        Student student3 = createStudent("test2", "test2", "test2", "m", new Date(), "0000", "0000");
        return new Student[] {student1, student2, student3};
    }
    private void createOneRow(Student student) {
        String sql = "INSERT INTO STUDENT " +
                "(ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME, GENDER, DATE_OF_BIRTH, " +
                "PASSPORT_SERIA, PASSPORT_NUMBER) " +
                "VALUES " +
                "(1, ?, ?, ?, ?, ?, ?, ?)";
        Object [] args = new Object[] {student.getFirstName(), student.getMiddleName(), student.getLastName(),
        student.getGender(), student.getDateOfBirth(), student.getPassportSeria(), student.getPassportNumber()};
        jdbcTemplate.update(sql, args);
    }


    private void createSeveralRows(Student ... students) {
        String sql = "INSERT INTO STUDENT " +
                "(ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME, GENDER, DATE_OF_BIRTH, " +
                "PASSPORT_SERIA, PASSPORT_NUMBER) " +
                "VALUES " +
                "(?, ?, ?, ?, ?, ?, ?, ?)";
        int count = 1;
        for (Student student : students) {
            Object[] args = new Object[]{count, student.getFirstName(), student.getMiddleName(), student.getLastName(),
                    student.getGender(), student.getDateOfBirth(), student.getPassportSeria(), student.getPassportNumber()};
            jdbcTemplate.update(sql, args);
            count++;
        }
    }

}
