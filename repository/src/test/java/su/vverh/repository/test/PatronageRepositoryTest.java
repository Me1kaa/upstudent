package su.vverh.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import su.vverh.model.Patronage;
import su.vverh.repository.PatronageRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by 1 on 06.03.2016.
 */
@ContextConfiguration(locations = "classpath:repository-test-context.xml")
public class PatronageRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private PatronageRepository patronageRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void insert_test_entity_exist_after_insert() {
        String name = "PETROV ARTEMIY VITALIEVICH";
        createOneRow(name);
        Patronage patronage = patronageRepository.findOne(1);
        Assert.assertNotNull(patronage);
        Assert.assertEquals(name, patronage.getFio());
    }

    @Test
    public void findAll_test_returns_all_rows() {
        String [] names = new String [] {"NAME1", "NAME2", "NAME3"};
        createSeveralRows(names);
        int rows = countRowsInTable("PATRONAGE");
        List<Patronage> patronages = patronageRepository.findAll();
        Assert.assertEquals(rows, patronages.size());

    }

    @Test
    public void save_test_saves_new_entity() {
        String name = "BELEVICH VITALIY FEDOROVICH";
        Patronage patronage = new Patronage();
        patronage.setFio(name);
        Assert.assertTrue(patronage.getId() <= 0);
        patronageRepository.save(patronage);
        Assert.assertTrue(patronage.getId() > 0);
        Patronage savedPatronage = entityManager.find(Patronage.class, patronage.getId());
        Assert.assertNotNull(savedPatronage);
        Assert.assertEquals(name, savedPatronage.getFio());
    }

    @Test
    public void save_test_updates_existing_entity() {
        String oldName = "OLDNAME";
        String newName = "NEWNAME";
        createOneRow(oldName);
        Patronage savedPatronage = entityManager.find(Patronage.class, 1);
        Assert.assertNotNull(savedPatronage);
        Assert.assertEquals(oldName, savedPatronage.getFio());

        savedPatronage.setFio(newName);
        patronageRepository.save(savedPatronage);
        Patronage updatedPatronage = entityManager.find(Patronage.class, 1);
        Assert.assertNotNull(updatedPatronage);
        Assert.assertNotEquals(oldName, updatedPatronage.getFio());
        Assert.assertEquals(newName, updatedPatronage.getFio());
    }

    @Test
    public void delete_test_delete_saved_entity() {
        String [] names = new String [] {"NAME1", "NAME2", "NAME3"};
        createSeveralRows(names);
        int oldCount = countRowsInTable("PATRONAGE");
        Patronage patronage = entityManager.find(Patronage.class, 1);
        Assert.assertNotNull(patronage);
        patronageRepository.delete(patronage);
        patronageRepository.flush();
        patronage = entityManager.find(Patronage.class, 1);
        Assert.assertNull(patronage);
        int newCount = countRowsInTable("PATRONAGE");
        Assert.assertEquals(oldCount, newCount + 1);
    }

    private void createSeveralRows(String ... names) {
        String sql = "INSERT INTO PATRONAGE (ID, FIO) VALUES (?, ?)";
        int count = 1;
        for (String name : names ) {
            jdbcTemplate.update(sql, count, name);
            count++;
        }
    }
    private void createOneRow(String name) {
        String sql = "INSERT INTO PATRONAGE (ID, FIO) VALUES (1, ?)";
        jdbcTemplate.update(sql, name);
    }

}
