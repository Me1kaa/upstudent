package su.vverh.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import su.vverh.model.EducationCenterType;
import su.vverh.repository.EducationCenterTypeRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by 1 on 06.03.2016.
 */
@ContextConfiguration(locations = "classpath:repository-test-context.xml")
public class EducationCenterTypeRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests{

    @Autowired
    private EducationCenterTypeRepository educationCenterTypeRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void insert_test_entity_exist_after_insert() {
        String type = "UNIVERSITY";
        createOneRow(type);
        EducationCenterType centerType = educationCenterTypeRepository.findOne(1);
        Assert.assertNotNull(centerType);
        Assert.assertEquals(type, centerType.getType());
    }

       @Test
    public void findAll_test_returns_all_rows() {
        String [] types = new String[]{"TYPE1", "TYPE2", "TYPE3"};
        createSeveralRows(types);
       int count = countRowsInTable("EDUCATION_CENTER_TYPE");
        List<EducationCenterType> typesList = educationCenterTypeRepository.findAll();
           Assert.assertEquals(count, typesList.size());

    }

    @Test
    public void save_test_saves_new_entity() {
        String type = "UNIVERSITY";
        EducationCenterType centerType = new EducationCenterType();
        centerType.setType(type);
        educationCenterTypeRepository.save(centerType);
        Assert.assertTrue(centerType.getId() > 0);
        EducationCenterType savedCenterType = entityManager.find(EducationCenterType.class, centerType.getId());
        Assert.assertNotNull(savedCenterType);
        Assert.assertEquals(type, savedCenterType.getType());

    }

    @Test
    public void save_test_updates_existing_entity() {
        String oldType = "UNIVERSITY";
        String newType = "ACADEMY";
        createOneRow(oldType);
        EducationCenterType type = entityManager.find(EducationCenterType.class, 1);
        Assert.assertNotNull(type);
        Assert.assertEquals(oldType, type.getType());

        type.setType(newType);
        educationCenterTypeRepository.save(type);
        EducationCenterType updatedType = entityManager.find(EducationCenterType.class, 1);
        Assert.assertNotNull(updatedType);
        Assert.assertNotEquals(oldType, updatedType.getType());
        Assert.assertEquals(newType, updatedType.getType());
    }

    @Test
    public void delete_test_delete_saved_entity() {
        String [] types = new String[]{"TYPE1", "TYPE2", "TYPE3"};
        createSeveralRows(types);
        int count = countRowsInTable("EDUCATION_CENTER_TYPE");
        EducationCenterType centerType = educationCenterTypeRepository.findOne(1);
        Assert.assertNotNull(centerType);
        educationCenterTypeRepository.delete(centerType);
        educationCenterTypeRepository.flush();
        centerType = educationCenterTypeRepository.findOne(1);
        Assert.assertNull(centerType);
        List<EducationCenterType> centerTypes = educationCenterTypeRepository.findAll();
        Assert.assertEquals(count, centerTypes.size() + 1);

    }

    private void createSeveralRows(String ... names) {
        String sql = "INSERT INTO EDUCATION_CENTER_TYPE (ID, TYPE) VALUES (?, ?)";
        int count = 1;
        for (String name : names) {
            jdbcTemplate.update(sql, count, name);
            count++;
        }
    }
    private void createOneRow(String name) {
        String sql = "INSERT INTO EDUCATION_CENTER_TYPE (ID, TYPE) VALUES (1, ?)";
        jdbcTemplate.update(sql, name);
    }
}
