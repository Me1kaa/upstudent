package su.vverh.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import su.vverh.model.Attestation;
import su.vverh.repository.AttestationRepository;

import java.util.Iterator;
import java.util.List;

/**
 * Created by 1 on 05.03.2016.
 */
@ContextConfiguration(locations = "classpath:repository-test-context.xml")
public class AttestationRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private AttestationRepository attestationRepository;

    @Test
    public void insert_test_entity_exist_after_insert() {
        String type = "TYPE1";
        createOneRow(type);
        Attestation attestation = attestationRepository.findOne(1);
        Assert.assertNotNull(attestation);
        Assert.assertEquals(type, attestation.getType());
    }

    @Test
    public void findAll_test_returns_all_rows() {
        String [] types = new String [] {"TYPE1", "TYPE2", "TYPE3"};
        createSeveralRows(types);
        int size = types.length;
        List<Attestation> attestations = attestationRepository.findAll();
        Assert.assertEquals(size, attestations.size());

    }

    @Test
    public void save_test_saves_new_entity() {
        Attestation attestation = new Attestation();
        attestation.setType("TYPE1");
        attestationRepository.save(attestation);
        Assert.assertTrue(attestation.getId() > 0);
        Attestation savedAttestation = attestationRepository.findOne(attestation.getId());
        Assert.assertNotNull(savedAttestation);
        Assert.assertEquals(attestation.getType(), savedAttestation.getType());
    }

    @Test
    public void save_test_updates_existing_entity() {
        String oldType = "TYPE1";
        String newType = "TYPE2";
        createOneRow(oldType);
        Attestation attestation = attestationRepository.findOne(1);
        Assert.assertNotNull(attestation);
        attestation.setType(newType);
        attestationRepository.save(attestation);
        Attestation updatedAttestation = attestationRepository.findOne(1);
        Assert.assertNotEquals(oldType, updatedAttestation.getType());
        Assert.assertEquals(newType, updatedAttestation.getType());
    }

    @Test
    public void delete_test_delete_saved_entity() {
        String [] types = new String[] {"TYPE1", "TYPE2"};
        createSeveralRows(types);
        int size = types.length;
        int rows = countRowsInTable("ATTESTATION");
        Assert.assertEquals(size, rows);
        Attestation attestation = attestationRepository.findOne(1);
        Assert.assertNotNull(attestation);
        attestationRepository.delete(attestation);
        attestationRepository.flush();
        attestation = attestationRepository.findOne(1);
        Assert.assertNull(attestation);
        rows = countRowsInTable("ATTESTATION");
        Assert.assertEquals(size, rows + 1);
    }

    private void createSeveralRows(String ... names) {
        String sql = "INSERT INTO ATTESTATION (ID, TYPE) VALUES (?, ?)";
        int count = 1;
        for (String name : names) {
            jdbcTemplate.update(sql, count, name);
            count++;
        }
    }
    private void createOneRow(String name) {
        String sql = "INSERT INTO ATTESTATION (ID, TYPE) VALUES (1, ?)";
        jdbcTemplate.update(sql, name);
    }

}
