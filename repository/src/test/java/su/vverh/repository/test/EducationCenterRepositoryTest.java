package su.vverh.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import su.vverh.model.EducationCenter;
import su.vverh.model.EducationCenterType;
import su.vverh.repository.EducationCenterRepository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by 1 on 05.03.2016.
 */
@ContextConfiguration(locations = "classpath:repository-test-context.xml")
public class EducationCenterRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private EducationCenterRepository educationCenterRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void insert_test_entity_exist_after_insert() {
        String name = "MGIMO";
        createOneRow(name);
        EducationCenter educationCenter = educationCenterRepository.findOne(1);
        Assert.assertNotNull(educationCenter);
        Assert.assertEquals(name, educationCenter.getName());
    }

    @Test
    public void findAll_test_returns_all_rows() {
        String [] names = new String [] {"MGIMO", "MGU", "MAI"};
        createSeveralRows(names);
        int size = names.length;
        List<EducationCenter> centers = educationCenterRepository.findAll();
        Assert.assertEquals(size, centers.size());
    }

    @Test
    public void save_test_saves_new_entity() {
        String sqlType = "INSERT INTO EDUCATION_CENTER_TYPE (TYPE) VALUES ('TYPE1')";
        jdbcTemplate.update(sqlType);
        EducationCenterType centerType = entityManager.find(EducationCenterType.class, 1);
        Assert.assertNotNull(centerType);
        String name = "MGU";
        EducationCenter educationCenter = new EducationCenter();
        educationCenter.setName(name);
        educationCenter.setType(centerType);

        educationCenterRepository.save(educationCenter);
        Assert.assertTrue(educationCenter.getId() > 0);
        EducationCenter savedEducationCenter = entityManager.find(EducationCenter.class, educationCenter.getId());
        Assert.assertNotNull(savedEducationCenter);
        Assert.assertEquals(name, savedEducationCenter.getName());
    }

    @Test
    public void save_test_updates_existing_entity() {
        String oldName = "MGIMO";
        String newName = "SIBERIAN UNIVERSITY";
        createOneRow(oldName);
        EducationCenter center = entityManager.find(EducationCenter.class, 1);
        Assert.assertNotNull(center);
        Assert.assertEquals(oldName, center.getName());

        center.setName(newName);
        educationCenterRepository.save(center);
        EducationCenter updatedCenter =  entityManager.find(EducationCenter.class, 1);
        Assert.assertNotNull(updatedCenter);
        Assert.assertNotEquals(oldName, updatedCenter.getName());
        Assert.assertEquals(newName, updatedCenter.getName());

    }

    @Test
    public void delete_test_delete_saved_entity() {
        String [] names = new String[] {"MGU", "MGIMO"};
        int size = names.length;
        createSeveralRows(names);
        EducationCenter center = entityManager.find(EducationCenter.class, 1);
        Assert.assertNotNull(center);
        educationCenterRepository.delete(center);
        center = entityManager.find(EducationCenter.class, 1);
        Assert.assertNull(center);
        List<EducationCenter> centers = educationCenterRepository.findAll();
        Assert.assertEquals(size, centers.size() + 1);
    }

    private void createSeveralRows(String ... names) {
        String sqlType = "INSERT INTO EDUCATION_CENTER_TYPE (ID, TYPE) VALUES (1, 'TYPE1')";
        jdbcTemplate.update(sqlType);
        String sql = "INSERT INTO EDUCATION_CENTER (ID, NAME, EDUCATION_CENTER_TYPE_ID) VALUES (?, ?, 1)";
        int count = 1;
        for (String name : names) {
            jdbcTemplate.update(sql, count, name);
            count++;
        }

    }
    private void createOneRow(String name) {
        String sqlType = "INSERT INTO EDUCATION_CENTER_TYPE (ID, TYPE) VALUES (1, 'TYPE1')";
        jdbcTemplate.update(sqlType);
        String sql = "INSERT INTO EDUCATION_CENTER (ID, NAME, EDUCATION_CENTER_TYPE_ID) VALUES (1, ?, 1)";
        jdbcTemplate.update(sql, name);
    }
}
