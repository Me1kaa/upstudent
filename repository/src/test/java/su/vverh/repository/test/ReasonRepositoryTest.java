package su.vverh.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import su.vverh.model.Reason;
import su.vverh.repository.ReasonRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by 1 on 06.03.2016.
 */
@ContextConfiguration(locations = "classpath:repository-test-context.xml")
public class ReasonRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private ReasonRepository reasonRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void insert_test_entity_exist_after_insert() {
        String name = "HARD_LIFE";
        createOneRow(name);
        Reason reason = reasonRepository.findOne(1);
        Assert.assertNotNull(reason);
        Assert.assertEquals(name, reason.getReason());
    }

    @Test
    public void findAll_test_returns_all_rows() {
        String [] names = new String[] {"R1", "R2", "R3"};
        createSeveralRows(names);
        int rows = countRowsInTable("REASON");
        List<Reason> reasons  = reasonRepository.findAll();
        Assert.assertEquals(rows, reasons.size());
    }

    @Test
    public void save_test_saves_new_entity() {
        String name = "HARD_LIFE";
        Reason reason = new Reason();
        reason.setReason(name);
        Assert.assertTrue(reason.getId() <= 0);

        reasonRepository.save(reason);
        Assert.assertTrue(reason.getId() > 0);
        Reason savedReason = entityManager.find(Reason.class, reason.getId());
        Assert.assertNotNull(savedReason);
        Assert.assertEquals(name, savedReason.getReason());
    }

    @Test
    public void save_test_updates_existing_entity() {
        String oldName = "HARD_LIFE";
        String newName = "EASY_LIFE";
        createOneRow(oldName);
        Reason reason = entityManager.find(Reason.class, 1);
        Assert.assertNotNull(reason);
        Assert.assertEquals(oldName, reason.getReason());

        reason.setReason(newName);
        reasonRepository.save(reason);
        reason = entityManager.find(Reason.class, 1);
        Assert.assertNotNull(reason);
        Assert.assertNotEquals(oldName, reason.getReason());
        Assert.assertEquals(newName, reason.getReason());

    }

    @Test
    public void delete_test_delete_saved_entity() {
        String [] names = new String[] {"R1", "R2", "R3"};
        createSeveralRows(names);
        int oldRows = countRowsInTable("REASON");
        Reason reason = entityManager.find(Reason.class, 1);
        Assert.assertNotNull(reason);

        reasonRepository.delete(reason);
        reasonRepository.flush();
        reason = entityManager.find(Reason.class, 1);
        Assert.assertNull(reason);
        int newRows = countRowsInTable("REASON");
        Assert.assertEquals(oldRows, newRows + 1);
    }

    private void createSeveralRows(String ... names) {
        String sql = "INSERT INTO REASON (ID, REASON) VALUES (?, ?)";
        int count = 1;
        for (String name : names) {
            jdbcTemplate.update(sql, count, name);
            count++;
        }
    }
    private void createOneRow(String name) {
        String sql = "INSERT INTO REASON (ID, REASON) VALUES (1, ?)";
        jdbcTemplate.update(sql, name);
    }
}
