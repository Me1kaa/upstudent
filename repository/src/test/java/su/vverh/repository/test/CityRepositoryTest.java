package su.vverh.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import su.vverh.model.City;
import su.vverh.model.Country;
import su.vverh.repository.CityRepository;
import su.vverh.repository.CountryRepository;
import java.util.Iterator;
import java.util.List;

/**
 * Created by shirval on 02.03.16.
 */
@ContextConfiguration(locations = "classpath:repository-test-context.xml")
public class CityRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private CityRepository cityRepository;
    @Autowired
    private CountryRepository countryRepository;

    @Test
    public void insert_entity_exists_after_insert() {
        String name = "������";
        createOneRow(name);
        City city = cityRepository.findOne(1);
        Assert.assertNotNull(city);
        Assert.assertEquals(name, city.getName());
    }

    @Test
    public void findAll_test_returns_all_rows() {
        String [] cities = new String[] {"������", "����", "����"};
        int size = cities.length;
        createSeveralRows(cities);
        List<City> cityList = cityRepository.findAll();
        Assert.assertEquals(size, cityList.size());
    }

    @Test
    public void save_test_save_new_city() {
        String sql = "INSERT INTO COUNTRY (NAME) VALUES ('������')";
        jdbcTemplate.update(sql);
        Country country = countryRepository.findByName("������");
        City city = new City();
        String cityName = "������";
        city.setName(cityName);
        city.setCountry(country);
        cityRepository.save(city);
        Assert.assertTrue(city.getId() > 0);
        City savedCity = cityRepository.findOne(city.getId());
        List<City> citites = cityRepository.findAll();
        Assert.assertNotNull(savedCity);
        Assert.assertEquals(cityName, savedCity.getName());
    }

    @Test
    public void save_test_updates_existing_city() {
        String oldName = "������";
        String newName = "����";
        createOneRow(oldName);
        City savedCity = cityRepository.findOne(1);
        Assert.assertEquals(oldName, savedCity.getName());
        savedCity.setName(newName);
        cityRepository.save(savedCity);
        City updatedCity = cityRepository.findOne(1);
        Assert.assertNotEquals(oldName, updatedCity.getName());
        Assert.assertEquals(newName, updatedCity.getName());
    }

    @Test
    public void delete_test_deletes_existing_entity() {
        String [] names = new String [] {"������","����"};
        createSeveralRows(names);
        City city = cityRepository.findOne(1);
        Assert.assertNotNull(city);
        Assert.assertEquals("������", city.getName());
        cityRepository.delete(city);
        city = cityRepository.findOne(1);
        Assert.assertNull(city);

    }

    private void createSeveralRows(String ... names) {
        String sqlCountry = "INSERT INTO COUNTRY (ID, NAME) VALUES (1, '������')";
        jdbcTemplate.update(sqlCountry);
        String sqlCity = "INSERT INTO CITY (NAME, COUNTRY_ID) " +
                "VALUES " +
                "(?, 1) ";
        for (String name : names) {
            jdbcTemplate.update(sqlCity, name);
        }
    }
    private void createOneRow(String name) {
        String sqlCountry = "INSERT INTO COUNTRY (ID, NAME) VALUES (1, '������')";
        jdbcTemplate.update(sqlCountry);
        String sql = "INSERT INTO CITY (ID, NAME, COUNTRY_ID) " +
                "VALUES " +
                "(1, ?, 1) ";
        jdbcTemplate.update(sql, name);
    }
}
