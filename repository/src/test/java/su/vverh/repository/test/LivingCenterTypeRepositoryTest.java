package su.vverh.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import su.vverh.model.LivingCenter;
import su.vverh.model.LivingCenterType;
import su.vverh.repository.LivingCenterTypeRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by 1 on 06.03.2016.
 */
@ContextConfiguration(locations = "classpath:repository-test-context.xml")
public class LivingCenterTypeRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests{

    @Autowired
    private LivingCenterTypeRepository livingCenterTypeRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void insert_test_entity_exist_after_insert() {
        String type = "HOSTEL";
        createOneRow(type);
        LivingCenterType centerType = livingCenterTypeRepository.findOne(1);
        Assert.assertNotNull(centerType);
        Assert.assertEquals(type, centerType.getType());
    }


    @Test
    public void findAll_test_returns_all_rows() {
        String [] types = new String[]{"TYPE1", "TYPE2", "TYPE3"};
        createSeveralRows(types);
        int count = countRowsInTable("LIVING_CENTER_TYPE");
        List<LivingCenterType> typesList = livingCenterTypeRepository.findAll();
        Assert.assertEquals(count, typesList.size());
    }

    @Test
    public void save_test_saves_new_entity() {
        String type = "ORPHANAGE";
        LivingCenterType centerType = new LivingCenterType();
        centerType.setType(type);
        livingCenterTypeRepository.save(centerType);
        Assert.assertTrue(centerType.getId() > 0);
        LivingCenterType savedCenterType = entityManager.find(LivingCenterType.class, centerType.getId());
        Assert.assertNotNull(savedCenterType);
        Assert.assertEquals(type, savedCenterType.getType());
    }

    @Test
    public void save_test_updates_existing_entity() {
        String oldType = "ORPHANAGE";
        String newType = "HOSTEL";
        createOneRow(oldType);
        LivingCenterType centerType = entityManager.find(LivingCenterType.class, 1);
        Assert.assertNotNull(centerType);
        Assert.assertEquals(oldType, centerType.getType());

        centerType.setType(newType);
        livingCenterTypeRepository.save(centerType);
        LivingCenterType updatedCenterType = entityManager.find(LivingCenterType.class, 1);
        Assert.assertNotNull(updatedCenterType);
        Assert.assertNotEquals(oldType, updatedCenterType.getType());
        Assert.assertEquals(newType, updatedCenterType.getType());
    }

    @Test
    public void delete_test_delete_saved_entity() {
        String [] types = new String[]{"TYPE1", "TYPE2", "TYPE3"};
        createSeveralRows(types);
        int count = countRowsInTable("LIVING_CENTER_TYPE");
        LivingCenterType centerType = entityManager.find(LivingCenterType.class, 1);
        Assert.assertNotNull(centerType);
        livingCenterTypeRepository.delete(centerType);
        livingCenterTypeRepository.flush();
        centerType = entityManager.find(LivingCenterType.class, 1);
        Assert.assertNull(centerType);
        List<LivingCenterType> centerTypes = livingCenterTypeRepository.findAll();
        Assert.assertEquals(count, centerTypes.size() + 1);
    }

    private void createSeveralRows(String ... names) {
        String sql = "INSERT INTO LIVING_CENTER_TYPE (ID, TYPE) VALUES (?, ?)";
        int count = 1;
        for (String name : names) {
            jdbcTemplate.update(sql, count, name);
            count++;
        }
    }
    private void createOneRow(String name) {
        String sql = "INSERT INTO LIVING_CENTER_TYPE (ID, TYPE) VALUES (1, ?)";
        jdbcTemplate.update(sql, name);
    }
}
