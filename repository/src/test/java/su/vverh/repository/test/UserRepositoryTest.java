package su.vverh.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import su.vverh.model.User;
import su.vverh.repository.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by 1 on 06.03.2016.
 */
@ContextConfiguration (locations = "classpath:repository-test-context.xml")
public class UserRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private UserRepository userRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void insert_test_entity_exist_after_insert() {
        int id = 1;
        String login = "LOGIN";
        String password = "PASSWORD";
        createOneRow(id, login, password);
        User user = userRepository.findOne(1);
        Assert.assertNotNull(user);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(password, user.getPassword());
    }

    @Test
    public void findAll_test_returns_all_rows() {
        createOneRow(1, "LOGIN1", "PASSWORD1");
        createOneRow(2, "LOGIN2", "PASSWORD2");
        int rows = countRowsInTable("USERS");
        List<User> users  = userRepository.findAll();
        Assert.assertEquals(rows, users.size());
    }

    @Test
    public void save_test_saves_new_entity() {
        String login = "LOGIN";
        String password = "PASSWORD";
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        User savedUser = entityManager.find(User.class, 1);
        Assert.assertNull(savedUser);

        userRepository.save(user);
        Assert.assertTrue(user.getId() > 0);
        savedUser = entityManager.find(User.class, 1);
        Assert.assertNotNull(savedUser);
        Assert.assertEquals(login, savedUser.getLogin());
        Assert.assertEquals(password, savedUser.getPassword());
    }

    @Test
    public void save_test_updates_existing_entity() {
        String oldLogin = "OLDLOGIN";
        String password = "PASSWORD";
        String newLogin = "NEWLOGIN";
        createOneRow(1, oldLogin, password);
        User user = entityManager.find(User.class, 1);
        Assert.assertNotNull(user);
        Assert.assertEquals(oldLogin, user.getLogin());

        user.setLogin(newLogin);
        userRepository.save(user);
        user = entityManager.find(User.class, 1);
        Assert.assertNotNull(user);
        Assert.assertNotEquals(oldLogin, user.getLogin());
        Assert.assertEquals(newLogin, user.getLogin());
        Assert.assertEquals(password, user.getPassword());
    }

    @Test
    public void delete_test_delete_saved_entity() {
        createOneRow(1, "LOGIN1", "PASSWORD1");
        createOneRow(2, "LOGIN2", "PASSWORD2");
        User user = entityManager.find(User.class, 1);
        int oldRows = countRowsInTable("USERS");
        Assert.assertNotNull(user);

        userRepository.delete(user);
        userRepository.flush();
        user = entityManager.find(User.class, 1);
        Assert.assertNull(user);
        int newRows = countRowsInTable("USERS");
        Assert.assertEquals(oldRows, newRows + 1);
    }

    private void createOneRow(int id, String login, String password) {
        String sql = "INSERT INTO USERS (ID, LOGIN, PASSWORD) VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, id, login, password);
    }
}
