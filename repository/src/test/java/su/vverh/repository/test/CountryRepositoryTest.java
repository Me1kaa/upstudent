package su.vverh.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import su.vverh.model.Country;
import su.vverh.repository.CountryRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by shirval on 02.03.16.
 */
@ContextConfiguration(locations = "classpath:repository-test-context.xml")
public class CountryRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private CountryRepository countryRepository;
    @PersistenceContext
    private EntityManager entityManager;


    @Test
    public void insert_test_entity_exist_after_insert() {
        String name = "RUSSIA";
        createOneRow(name);
        Country country = countryRepository.findOne(1);
        Assert.assertEquals(name, country.getName());
    }

    @Test
    public void findByName_test_entity_is_findable_by_name() {
        String name = "RUSSIA";
        createOneRow(name);
        Country country = countryRepository.findByName(name);
        Assert.assertNotNull(country);
        Assert.assertEquals(name, country.getName());
    }

    @Test
    public void findAll_test_returns_all_rows() {
        String [] names = new String[] {"RUSSIA", "UKRAINE", "KAZAHSTAN"};
        createSeveralRows(names);
        int rows = countRowsInTable("COUNTRY");
        List<Country> countries = countryRepository.findAll();
        Assert.assertEquals(rows, countries.size());
    }

    @Test
    public void save_test_saves_new_country() {
        Country newCountry = new Country();
        String name = "UKRAINE";
        newCountry.setName(name);
        countryRepository.save(newCountry);
        Assert.assertTrue(newCountry.getId() > 0);
        Country savedCountry = entityManager.find(Country.class, newCountry.getId());
        Assert.assertEquals(newCountry.getName(), savedCountry.getName());
        Assert.assertEquals(newCountry.getId(), savedCountry.getId());
    }

    @Test
    public void save_test_updates_existing_country() {
        String oldName = "BOLIVIA";
        String newName = "BELORUSSIA";
        createOneRow(oldName);
        Country savedCountry = countryRepository.findByName(oldName);
        Assert.assertNotNull(savedCountry);
        savedCountry.setName(newName);
        countryRepository.save(savedCountry);
        Country updatedCountry = countryRepository.findByName(oldName);
        Assert.assertNull(updatedCountry);
        updatedCountry = countryRepository.findByName(newName);
        Assert.assertNotNull(updatedCountry);
    }

    @Test
    public void delete_test_delete_saved_country() {
        String name = "USA";
        createOneRow(name);
        Country savedCountry = entityManager.find(Country.class, 1);
        Assert.assertNotNull(savedCountry);
        countryRepository.delete(savedCountry);
        countryRepository.flush();
        savedCountry =  entityManager.find(Country.class, 1);
        Assert.assertNull(savedCountry);
    }

    private void createSeveralRows(String ... names) {
        String sql = "INSERT INTO COUNTRY (ID, NAME) " +
                "VALUES " +
                "(?, ?) ";
        int count = 1;
        for (String name : names) {
            jdbcTemplate.update(sql, count, name);
            count++;
        }
    }
    private void createOneRow(String name) {
        String sql = "INSERT INTO COUNTRY (ID, NAME) " +
                "VALUES " +
                "(1, ?) ";
        jdbcTemplate.update(sql, name);
    }
}
