package su.vverh.repository.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import su.vverh.model.Adress;
import su.vverh.repository.AdressRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by shirval on 02.03.16.
 */
@ContextConfiguration(locations = "classpath:repository-test-context.xml")
public class AdressRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private AdressRepository adressRepository;
    @PersistenceContext
    private EntityManager entityManager;


    @Test
    public void insert_test_entity_exist_after_insert() {
        String name = "MOSCOW, LENINA STREET, 1, 15";
        createOneRow(name);
        Adress adress = adressRepository.findOne(1L);
        Assert.assertNotNull(adress);
        Assert.assertEquals(name, adress.getAdress());
    }


    @Test
    public void findAll_test_returns_all_rows() {
        String [] adresses = new String[] {"MOSCOW, LENINA STREET, 1, 15", "TULA, SEROVA STREET, 44, 55"};
        createSeveralRows(adresses);
        int size = adresses.length;
        List<Adress> adressList = adressRepository.findAll();
        Assert.assertEquals(size, adressList.size());
    }

    @Test
    public void save_test_saves_new_entity() {
        String name = "MOSCOW, LENINA STREET, 1, 15";
        Adress adress = new Adress();
        adress.setAdress(name);
        adressRepository.save(adress);
        Assert.assertTrue(adress.getId() > 0);
        Adress savedAdress = adressRepository.findOne(adress.getId());
        Assert.assertNotNull(savedAdress);
        Assert.assertEquals(adress.getAdress(), savedAdress.getAdress());
    }

    @Test
    public void save_test_updates_existing_entity() {
        String oldName = "MOSCOW, LENINA STREET, 1, 15";
        String newName =  "TULA, SEROVA STREET, 44, 55";
        createOneRow(oldName);
        Adress adress = adressRepository.findOne(1L);
        Assert.assertNotNull(adress);
        Assert.assertEquals(oldName, adress.getAdress());
        adress.setAdress(newName);
        adressRepository.save(adress);
        Adress updatedAdress = adressRepository.findOne(1L);
        Assert.assertNotEquals(oldName, updatedAdress.getAdress());
        Assert.assertEquals(newName, updatedAdress.getAdress());
    }

    @Test
    public void delete_test_delete_saved_entity() {
        String [] adresses = new String[] {"MOSCOW, LENINA STREET, 1, 15", "TULA, SEROVA STREET, 44, 55"};
        createSeveralRows(adresses);
        int rows = countRowsInTable("ADRESS");
        Adress adress = adressRepository.findOne(1L);
        Assert.assertNotNull(adress);
        adressRepository.delete(adress);
        adressRepository.flush();
        int newRows = countRowsInTable("ADRESS");
        Assert.assertEquals(rows, newRows + 1);
        adress = adressRepository.findOne(1L);
        Assert.assertNull(adress);
    }

    private void createSeveralRows(String ... adresses) {
        String sql = "INSERT INTO ADRESS (ID, ADRESS) VALUES (?, ?)";
        int count = 1;
        for (String adress : adresses) {
            jdbcTemplate.update(sql, count, adress);
            count++;
        }
    }
    private void createOneRow(String adress) {
        String sql = "INSERT INTO ADRESS (ID, ADRESS) VALUES (1, ?)";
        jdbcTemplate.update(sql, adress);
    }
}
