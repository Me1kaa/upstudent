package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.UserRole;

/**
 * Created by shirval on 02.03.16.
 */
public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {
}
