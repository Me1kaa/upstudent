package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.MaritalStatus;

/**
 * Created by shirval on 02.03.16.
 */
public interface MaritalStatusRepository extends JpaRepository<MaritalStatus, Integer> {
}
