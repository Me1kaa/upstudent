package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.Patronage;

/**
 * Created by shirval on 02.03.16.
 */
public interface PatronageRepository extends JpaRepository<Patronage, Integer> {
}
