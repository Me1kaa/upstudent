package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.Student;

/**
 * Created by shirval on 01.03.16.
 */
public interface StudentRepository extends JpaRepository<Student, Integer> {
}
