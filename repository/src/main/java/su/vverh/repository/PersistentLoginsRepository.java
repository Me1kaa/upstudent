package su.vverh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.PersistentLogins;

/**
 * Created by shirval on 24.03.16.
 */
public interface PersistentLoginsRepository extends JpaRepository<PersistentLogins, String> {
}
