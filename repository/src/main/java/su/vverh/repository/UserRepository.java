package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.User;

/**
 * Created by shirval on 02.03.16.
 */
public interface UserRepository extends JpaRepository<User, Integer> {

    User findByLogin(String login);
}
