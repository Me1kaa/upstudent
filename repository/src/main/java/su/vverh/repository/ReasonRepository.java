package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.Reason;

/**
 * Created by shirval on 02.03.16.
 */
public interface ReasonRepository extends JpaRepository<Reason, Integer> {
}
