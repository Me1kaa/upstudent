package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.UserGroup;

/**
 * Created by shirval on 02.03.16.
 */
public interface UserGroupRepository extends JpaRepository<UserGroup, Integer> {
}
