package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.EducationCenter;

/**
 * Created by shirval on 02.03.16.
 */
public interface EducationCenterRepository extends JpaRepository<EducationCenter, Integer> {
}
