package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.Attestation;

/**
 * Created by shirval on 02.03.16.
 */
public interface AttestationRepository extends JpaRepository<Attestation, Integer> {
}
