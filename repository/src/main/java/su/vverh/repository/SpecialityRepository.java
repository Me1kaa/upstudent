package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.Speciality;

/**
 * Created by shirval on 02.03.16.
 */
public interface SpecialityRepository extends JpaRepository<Speciality, Integer> {
}
