package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.Country;


/**
 * Created by shirval on 02.03.16.
 */
public interface CountryRepository extends JpaRepository<Country, Integer> {

    Country findByName(String name);

}
