package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.StudentLife;


/**
 * Created by shirval on 02.03.16.
 */
public interface StudentLifeRepository extends JpaRepository<StudentLife, Integer> {
}
