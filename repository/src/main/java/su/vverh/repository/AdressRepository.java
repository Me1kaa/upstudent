package su.vverh.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.Adress;

/**
 * Created by shirval on 01.03.16.
 */
public interface AdressRepository extends JpaRepository<Adress, Long> {
}
