package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.LivingCenterType;

/**
 * Created by shirval on 02.03.16.
 */
public interface LivingCenterTypeRepository extends JpaRepository<LivingCenterType, Integer> {
}
