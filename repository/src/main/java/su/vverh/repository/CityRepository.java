package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.City;

/**
 * Created by shirval on 02.03.16.
 */
public interface CityRepository extends JpaRepository<City, Integer> {

    City findByNameAndCountryId(String name, int countryId);
}
