package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.EducationCenterType;

/**
 * Created by shirval on 02.03.16.
 */
public interface EducationCenterTypeRepository extends JpaRepository<EducationCenterType, Integer> {
}
