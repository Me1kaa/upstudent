package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.LivingCenter;

/**
 * Created by shirval on 02.03.16.
 */
public interface LivingCenterRepository extends JpaRepository<LivingCenter, Integer> {
}
