package su.vverh.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import su.vverh.model.StudentStudy;

/**
 * Created by shirval on 02.03.16.
 */
public interface StudentStudyRepository extends JpaRepository<StudentStudy, Integer> {
}
