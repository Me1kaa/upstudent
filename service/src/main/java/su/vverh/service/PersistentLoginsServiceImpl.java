package su.vverh.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import su.vverh.model.PersistentLogins;
import su.vverh.repository.PersistentLoginsRepository;

import java.util.List;

/**
 * Created by shirval on 24.03.16.
 */
@Service
public class PersistentLoginsServiceImpl implements PersistentLoginsService {

    @Autowired
    private PersistentLoginsRepository persistentLoginsRepository;

    @Override
    public List<PersistentLogins> findAll() {
        return persistentLoginsRepository.findAll();
    }
}
