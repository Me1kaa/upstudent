package su.vverh.service;

import su.vverh.model.User;

/**
 * Created by 1 on 14.03.2016.
 */
public interface UserService {

    User findByLogin(String login);
}
