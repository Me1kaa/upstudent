package su.vverh.service;

import su.vverh.model.PersistentLogins;

import java.util.List;

/**
 * Created by shirval on 24.03.16.
 */
public interface PersistentLoginsService {

    List<PersistentLogins> findAll();
}
